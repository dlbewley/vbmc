PACKAGE?=''

pyenv:
	./setup-pyenv

clean:
	rm -rf *egg-info dist/* build/*

realclean: clean
	rm -rf pyenv venv; \
    "$$PACKAGE" && pyenv uninstall "$$PACKAGE"

superclean: realclean
	"$$PACKAGE" && pyenv virtualenv-delete "$$PACKAGE"
